

import parking.notificador.Notificador;

public class NotificadorTelegram implements Notificador{
	
	private String message = "Mensaje enviado desde TELEGRAM";

	@Override
	public void notificar() {
		System.out.println(message);
	}

}
