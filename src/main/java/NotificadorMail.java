

import parking.notificador.Notificador;

public class NotificadorMail implements Notificador{
	
	private String message = "Mensaje enviado por MAIL";

	@Override
	public void notificar() {
		System.out.println(message);
	}

}
